class ExtendSerializerContextMixin:
    """
    Mixin for extending serializer context
    Working with `GenericAPIView`
    """
    def get_serializer_context(self):
        context: dict = super().get_serializer_context()
        context['user'] = self.request.user
        return context

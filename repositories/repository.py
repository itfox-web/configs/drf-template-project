from abc import ABC
from typing import Any, Optional, Union
from django.core.exceptions import ObjectDoesNotExist



class Repository(ABC): 
    """Class-parent for all child repositories classes""" 
    
    @staticmethod
    def get_object_or_none(cls: Any, pk: int = None, select_related_fields: tuple[str] = None, unpack_related_fields=False, **kwargs) -> Any:
        if not kwargs and pk:
            kwargs = dict(pk=pk)
        elif kwargs and pk:
            kwargs['pk'] = pk
        try:
            if select_related_fields:
                obj = cls.objects.select_related(
                    *select_related_fields).get(**kwargs)
                related_objects = [getattr(obj, field)
                                for field in select_related_fields]
                related_objects.insert(0, obj)
                return related_objects if unpack_related_fields else obj
            return cls.objects.get(**kwargs)
        except ObjectDoesNotExist:
            if select_related_fields:
                length = len(select_related_fields) + 1
                return tuple({}.get(None) for i in range(length))
            return None

    @staticmethod
    def validate_number(value: Any):
        """Method for standart type validating"""
        if isinstance(value, int):
            return True
        elif isinstance(value, str):
            try:
                int(value)
                return True
            except ValueError:
                return False
        return False

    @staticmethod
    def get_kwargs(data: dict, keys: Union[list, tuple]) -> dict:
        """Finding values in a dict with passed fields"""
        kwargs = dict()
        for key in keys:
            value = data.get(key)
            if value:
                kwargs.update({key: value})
        return kwargs